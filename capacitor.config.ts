import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'tutorial.react',
  appName: 'tutorial-react',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
