import { DECREMENT, INCREMENT_5, DECREMENT_5, INCREMENT, RESET } from "../../action-types";

export interface IContadorActionReducer {
    type: string;
    payload?: number;
}

export type IContador = number;

const initialState: IContador = 0

export default function contadorReducer(state = initialState, action: IContadorActionReducer) {
    const payloadData = action.payload ? action.payload : 0;
    switch (action.type) {
        case INCREMENT:
            return state + 1
        case DECREMENT:
            return state -1
        case INCREMENT_5:
            return state + payloadData;
        case DECREMENT_5:
            return state - payloadData;
        case RESET:
            return initialState;
        default:
            return state;
    }
}