import { combineReducers } from '@reduxjs/toolkit';
import contadorReducer, { IContador } from './contador/contador.reducer';

export interface AppStore {
    contador: IContador;
}

const reducer = combineReducers<AppStore>({
    contador: contadorReducer
});

export default reducer;