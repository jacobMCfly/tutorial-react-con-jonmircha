import { configureStore } from '@reduxjs/toolkit'
import reducer from './reducers'

// ======================================================================
// CONFIGURA STORE
// ====================================================================== 

const store = configureStore({ reducer });

// ======================================================================
// EVENTO DEL REDUCER
// ====================================================================== 

store.subscribe(() => console.log(store));

export default store;