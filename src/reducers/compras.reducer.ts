import { TYPES } from './actions/compras.actions';

export const comprasDefaultState = {
    productos: [
        { id: 1, nombre: 'producto 1', precio: 100 },
        { id: 2, nombre: 'producto 2', precio: 200 },
        { id: 3, nombre: 'producto 3', precio: 300 },
        { id: 4, nombre: 'producto 4', precio: 400 },
        { id: 5, nombre: 'producto 5', precio: 500 },
        { id: 6, nombre: 'producto 6', precio: 600 },
        { id: 7, nombre: 'producto 7', precio: 700 }
    ],
    carrito: []
};

export function comprasReducer(state: any, action: { type: string, payload?: any }) {
    switch (action.type) {
        case TYPES.AGREGAR_PRODUCTO_AL_CARRITO: {
            let nuevoProducto = state.productos.find((producto: any) => producto.id === action.payload);
            let articuloEnCarrito = state.carrito.find((producto: any) => producto.id === nuevoProducto.id);

            return articuloEnCarrito ? {
                ...state,
                carrito: state.carrito.map((articulo: any) =>
                    articulo.id === nuevoProducto.id ? { ...articulo, cantidad: articulo.cantidad + 1 } : articulo
                )
            } : {
                ...state,
                carrito: [...state.carrito, { ...nuevoProducto, cantidad: 1 }]
            }; // retorna el estado y en el carrito hace una copia del existente y agrega el nuevo
        }
        case TYPES.ELIMINAR_PRODUCTO_DEL_CARRITO: {
            let produtoParaEliminar = state.carrito.find((producto: any) => producto.id === action.payload);
            return (produtoParaEliminar.cantidad > 1) ?
                {
                    ...state,
                    carrito: state.carrito.map((articulo: any) => {
                        return (articulo.id === action.payload)
                            ? { ...articulo, cantidad: articulo.cantidad - 1 } : articulo
                    })
                }
                : {
                    ...state,
                    carrito: state.carrito.filter((articulo: any) => articulo.id !== action.payload)
                };
        }
        case TYPES.ELIMINAR_TODO_DEL_CARRITO: {
            return {
                ...state,
                carrito: state.carrito.filter((articulo: any) => {
                    return articulo.id !== action.payload
                }) // regresa todos los articulos que no sean el que se selecciono para eliminar 
            };
        }
        case TYPES.LIMPIAR_CARRITO: { return comprasDefaultState; }
        default:
            return state;

    }
} 
