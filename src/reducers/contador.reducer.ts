import { TYPES } from "./actions/contador.actions";

// ======================================================================
//  Reducer
// ====================================================================== 

export const contadorReducerDefaultState = {
    contador: 0
};

export const contadorInit = (): any => {
    return {
        contador: contadorReducerDefaultState.contador + 1
    }
};

export function contadorReducer(state: any, action: { type: string, payload?: any }) {
    switch (action.type) {
        case TYPES.INCREMENTAR:
            return { contador: state.contador + 1 };
        case TYPES.INCREMENTAR_5:
            return { contador: state.contador + action.payload };
        case TYPES.DECREMENTAR:
            return { contador: state.contador - 1 };
        case TYPES.DECREMENTAR_5:
            return { contador: state.contador - action.payload };
        case TYPES.RESET:
            return contadorReducerDefaultState;
        default:
            return state;
    }
} 