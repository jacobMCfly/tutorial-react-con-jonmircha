import React from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import Index from './pages/Index';
import Dashboard from './pages/Dashboard/Dashboard';
import AcercaDePage from './pages/acercade/acercade';
import Error404 from './pages/Error404';
import RutaPrivada from './security/RutaPrivada';
import { Provider } from 'react-redux';
import store from './redux/store';


const App: React.FC = () => {
  return (
    <Provider store={store}> 
      <BrowserRouter>
        <Routes>
          {/* Error 404 */}
          <Route path="*" element={<Error404 />} ></Route>
          {/* Redirección */}
          <Route path="/" element={<Navigate replace to="inicio" />} />
          {/* Rutas */}
          <Route path="inicio" element={<Index />} />
          <Route path="inicio/dashboard" element={<Dashboard />} />
          <Route path="acercade" element={<RutaPrivada element={AcercaDePage} />} />
        </Routes >
      </BrowserRouter>
    </Provider>
  );
}


export default App;


