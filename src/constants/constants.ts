const prod = {
    url: { API_URL: 'https://' },
    produccion: true,
    aplicacion: {
        acciones: '/acciones/api/',
        creditos: '/creditos/api/',
        agenda: '/agenda/api'
    }
};

const dev = {
    url: { API_URL: 'http://localhost:8000' },
    produccion: false,
    aplicacion: {
        acciones: '/acciones/api/',
        creditos: '/creditos/api/',
        agenda: '/agenda/api'
    }
};


export const config = process.env.NODE_ENV === 'development' ? dev : prod;