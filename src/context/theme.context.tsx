import React, { createContext } from 'react';

// Context 
const initialTheme = 'light'
const ThemeCustomContext = createContext<any>(initialTheme);

// provider
interface IThemeCustomProviderProps {
    children?: React.ReactNode | null | string | undefined;
}

const ThemeCustomProvider: React.FC<IThemeCustomProviderProps> = ({ children }) => {
    const [theme, setTheme] = React.useState(initialTheme);

    const cambiarTema = (e: React.ChangeEvent<any>) => {
        const tema = e.target.value;
        setTheme(tema);
    };

    const data = { theme, cambiarTema };

    return (
        <ThemeCustomContext.Provider value={data}>{children}</ThemeCustomContext.Provider>
    );
};

export { ThemeCustomProvider };
export default ThemeCustomContext;