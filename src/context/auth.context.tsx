import React from 'react';

// ======================================================================
// Context
// ====================================================================== 

interface IAuthContext {
    autenticar?: (arg: React.ChangeEvent<any>) => void;
    autenticado?: boolean;
}

const initialAuth = {
    autenticado: false,
    autenticar: () => { return null }
};

const AuthContext = React.createContext<IAuthContext>(initialAuth);

// ======================================================================
// Provider
// ====================================================================== 

interface IAuthProviderProps {
    children?: React.ReactNode | string | null;
}

const AuthProvider: React.FC<IAuthProviderProps> = ({ children }) => {
    const [autenticado, setAuth] = React.useState(initialAuth.autenticado);

    const autenticar = (e: React.ChangeEvent<any>) => {
        setAuth(!autenticado)
    };

    const data: IAuthContext = { autenticado, autenticar };

    return (
        <AuthContext.Provider value={data}> {children} </AuthContext.Provider>
    );
};


export { AuthProvider };
export default AuthContext;