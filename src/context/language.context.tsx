import React, { useState } from 'react';

// Variable para el cambio de idioma normalmente archivo separados por
const translations = {
    es: {
        header: {
            title: 'Mi aplicación CON context',
            theme: {
                light: 'Claro',
                dark: 'Oscuro'
            }
        },
        main: {
            welcome: 'Bienvenid@ invitado',
            hello: 'Hola usuari@',
            content: 'Mi contenido principal'
        },
        footer: {
            title: 'Mi footer'
        },
        button: {
            login: 'Iniciar sesión',
            logout: 'Cerrar sesión'
        },
    },
    en: {
        header: {
            title: 'Mi application with context',
            theme: {
                light: 'Light',
                dark: 'Dark'
            }
        },
        main: {
            welcome: 'Welcome guess',
            hello: 'Hello user',
            content: 'My main content'
        },
        footer: {
            title: 'my footer'
        },
        button: {
            login: 'Log in',
            logout: 'Log out'
        },
    }
};

// Context 
interface ILanguageContext {
    language: string;
    cambiarLenguaje?: ((arg: React.ChangeEvent<any>) => void) | undefined;
    texts?: any | undefined;
}

const defaultLanguage = 'es';
const LanguageContext = React.createContext<ILanguageContext>({
    language: defaultLanguage,
    cambiarLenguaje: undefined,
    texts: undefined,
});

// Provider  
interface ILanguageProviderProps {
    children?: React.ReactNode | null | undefined;
}

const LanguageProvider: React.FC<ILanguageProviderProps> = ({ children }) => {
    const [language, setLanguage] = React.useState<keyof typeof translations>(defaultLanguage);
    const [texts, setTexts] = React.useState(translations[language]);

    const cambiarLenguaje = (e: React.ChangeEvent<any>) => {
        const valorEvento = e.target.value as keyof typeof translations;
        setLanguage(valorEvento);
        setTexts(translations[valorEvento]);
    }

    const data = { language, texts, cambiarLenguaje };

    return (
        <LanguageContext.Provider value={data}>{children}</LanguageContext.Provider>
    );
};

export { LanguageProvider };
export default LanguageContext;



