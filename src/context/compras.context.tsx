import React from 'react';
import { TYPES } from '../reducers/actions/compras.actions';
import { comprasDefaultState, comprasReducer } from '../reducers/compras.reducer';
import { api } from '../services/api-creditos';

// ======================================================================
// CONTEXT
// ====================================================================== 

interface ICarritoContext {
    productos: any;
    carrito: any;
    agregarProductoAlCarrito: (id?: string | number) => void;
    eliminarProductoDelCarrito: (id?: string | number, todos?: boolean) => void;
    limpiarCarrito: () => void;
}

const carritoDefaultContext: ICarritoContext = {
    productos: comprasDefaultState.productos,
    carrito: comprasDefaultState.carrito,
    agregarProductoAlCarrito: () => { },
    eliminarProductoDelCarrito: () => { },
    limpiarCarrito: () => { }
}

const CarritoContext = React.createContext(carritoDefaultContext);

// ======================================================================
// PROVIDER
// ======================================================================  

interface ICarritoProviderProps {
    children: React.ReactNode | string;
}


const CarritoProvider: React.FunctionComponent<ICarritoProviderProps> = ({ children }) => {
    const [state, dispatch] = React.useReducer(comprasReducer, carritoDefaultContext);
    const { productos, carrito } = state;


    React.useEffect(() => {
        const obtenerDatos = async () => {
            const response = await api.lista('menu')
            if (response) {
                console.log(response.data)
            }
        }
        obtenerDatos();
    }, []);

    const agregarProductoAlCarrito = (id?: string | number) => {
        dispatch({
            type: TYPES.AGREGAR_PRODUCTO_AL_CARRITO,
            payload: id
        });
    };

    const eliminarProductoDelCarrito = (id?: string | number, todos = false) => {
        if (todos) {
            dispatch({ type: TYPES.ELIMINAR_TODO_DEL_CARRITO, payload: id });
        } else {
            dispatch({ type: TYPES.ELIMINAR_PRODUCTO_DEL_CARRITO, payload: id });
        }
    };

    const limpiarCarrito = () => {
        dispatch({ type: TYPES.LIMPIAR_CARRITO });
    };

    const data = {
        productos,
        carrito,
        agregarProductoAlCarrito,
        eliminarProductoDelCarrito,
        limpiarCarrito
    };

    return (
        <CarritoContext.Provider value={data}>{children}</CarritoContext.Provider>
    );
};



export { CarritoProvider };

export default CarritoContext;