import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { reset, restar, restar5, sumar, sumar5 } from '../../../redux/actions/contador.actions';
import { AppStore } from '../../../redux/reducers';

const ContadorReduxComponent: React.FC = () => {
  const state = useSelector((state: AppStore) => state);
  const dispatch = useDispatch()
  const { contador } = state;
  return (
    <div>
      <h2>Contador Redux</h2>
      <nav>
        <button onClick={() => dispatch(sumar5())}>+5</button>
     <button onClick={() => dispatch(sumar()) }>+1</button>
        <button onClick={() => dispatch(reset()) }>0</button>
        <button onClick={ () => dispatch(restar())}>-1</button>
        <button onClick={() => dispatch(restar5()) }>-5</button>  
      </nav>
      <h3>{contador}</h3>
    </div>
  );
};

export default ContadorReduxComponent;
