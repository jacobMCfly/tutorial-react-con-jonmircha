import * as React from 'react';
import ContadorReduxComponent from './components/contador-redux.component';

interface IReduxComponentProps {
}

const ReduxComponent: React.FC<IReduxComponentProps> = (props) => {
    return (

        <div style={{ textAlign: 'center' }}>
            <h1>Redux</h1>
            <hr />
            <ContadorReduxComponent />
        </div>


    );
};

export default ReduxComponent;
