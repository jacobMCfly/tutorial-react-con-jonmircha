import { Box, DialogTitle, IconButton, Dialog } from '@mui/material';
import { styled } from '@mui/material/styles';

import CloseIcon from '@mui/icons-material/Close';
import * as React from 'react';

const DialogComponent = Dialog

export interface IModalComponentProps {
  id: string;
  children?: React.ReactNode;
  onClose: () => void;
}

function DialogTituloComponent(props: IModalComponentProps) {
  const { children, onClose, ...other } = props;

  return (
    <>
      <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
        {children}
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
            <CloseIcon />
          </IconButton>
        ) : null}
      </DialogTitle>
    </>
  );
}


export { DialogComponent, DialogTituloComponent }