import { Button, Box, DialogContent, useMediaQuery, useTheme } from '@mui/material';
import * as React from 'react';
import { FormularioHooks } from '../FormularioHooks';
import { DialogComponent, DialogTituloComponent } from './ModalComponent';
// import { makeStyles } from '@mui/styles';

// const useStyles = makeStyles(() => ({
//     appBar: {
//         paddingTop: `env(safe-area-inset-top)`,
//         paddingLeft: `env(safe-area-inset-left)`,
//         paddingRight: `env(safe-area-inset-right)`
//     },
// }));

export interface IModalsProps {
}

function Modals(props: IModalsProps) {
    // const clases = useStyles();

    const [open, setOpen] = React.useState(false);
    const [dialogFullScreen, setDialogFullScreen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const theme = useTheme();
    const sm = useMediaQuery(theme.breakpoints.down("sm"));
    React.useEffect(() => {
        sm === false ? setDialogFullScreen(false) : setDialogFullScreen(true);
    }, [sm]);



    return (
        <>
            <Button variant="contained" onClick={handleClickOpen}> Modal 1 </Button>
            <Box sx={{ maxWidth: '100%' }}>
                <DialogComponent
                    fullScreen={dialogFullScreen}
                    fullWidth={true}
                    onClose={handleClose}
                    aria-labelledby="customized-dialog-title"
                    open={open}
                >
                    <DialogTituloComponent id="customized-dialog-title" onClose={handleClose}>
                        Formulario con hooks
                    </DialogTituloComponent>
                    <DialogContent dividers>
                        <FormularioHooks onClose={handleClose} />
                    </DialogContent>
                </DialogComponent>
            </Box>
        </>
    )


}


export { Modals }