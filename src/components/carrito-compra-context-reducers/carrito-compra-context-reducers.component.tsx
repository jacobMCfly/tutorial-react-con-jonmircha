import * as React from 'react';
import { CarritoProvider } from '../../context/compras.context';
import CarritoDeComprasComponent from './components/carrito-de-compras/carrito-de-compras.component';

interface ICCarritoContextReducersComponentProps { }

const CarritoContextReducersComponent: React.FC<ICCarritoContextReducersComponentProps> = (props) => {
    return (
        <CarritoProvider>
            <CarritoDeComprasComponent />
        </CarritoProvider>
    );
};

export default CarritoContextReducersComponent;
