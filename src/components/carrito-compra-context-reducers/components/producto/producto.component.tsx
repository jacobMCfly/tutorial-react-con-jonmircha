import * as React from 'react';
import CarritoContext from '../../../../context/compras.context';
import './producto.component.css';

interface IProductoComponentProps {
    data: {
        id?: string | number;
        nombre: string;
        precio: string | number;
    };
}
const ProductoComponent: React.FC<IProductoComponentProps> = ({ data }) => {
    let { id, nombre, precio } = data;
    const { agregarProductoAlCarrito } = React.useContext(CarritoContext);
    return (

        <div className='tarjeta'>
            <h4>{nombre}</h4>
            <h5>${precio}.00</h5>
            <button onClick={() => agregarProductoAlCarrito(id)}>Agregar</button>
        </div>
    );
};

export default ProductoComponent;
