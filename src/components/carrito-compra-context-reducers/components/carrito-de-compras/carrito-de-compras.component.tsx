import * as React from 'react';
import CarritoContext from '../../../../context/compras.context';
import { TYPES } from '../../../../reducers/actions/compras.actions';
import { comprasDefaultState, comprasReducer } from '../../../../reducers/compras.reducer';
import CarritoArticuloComponent from '../carrito-articulo/carrito-articulo.component';
import ProductoComponent from '../producto/producto.component';
import './carrito-de-compras.component.css';

interface ICarritoDeComprasComponentProps {
}

const CarritoDeComprasComponent: React.FC<ICarritoDeComprasComponentProps> = (props) => {
    const {
        productos,
        carrito,
        limpiarCarrito
    } = React.useContext(CarritoContext);

    return (
        <>
            <div style={{ textAlign: 'center', marginTop: '1rem' }}>
                <h2>Carrito de compras conrtext + reducers</h2>
                <h3>Productos</h3>
                <article className="box grid-responsive">
                    {productos.map(
                        (producto: any) =>
                            <ProductoComponent
                                key={producto.id}
                                data={producto}
                            />
                    )}
                </article>
                <h3>Carrito</h3>
                <article className="box">
                    <button onClick={limpiarCarrito}>Limpiar carrito</button>
                    {carrito.map((articulo: any) =>
                        <CarritoArticuloComponent
                            key={articulo.id}
                            data={articulo}
                        />
                    )}
                </article>
            </div>

        </>
    );
};

export default CarritoDeComprasComponent;
