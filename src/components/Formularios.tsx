import React, { useState } from 'react';
import {
    TextField,
    Box,
    Radio,
    FormControl,
    FormLabel,
    RadioGroup,
    FormControlLabel,
    InputLabel,
    Select,
    MenuItem,
    Checkbox
} from '@mui/material';
import { SelectChangeEvent } from '@mui/material/Select';
import LoadingButton from '@mui/lab/LoadingButton';
import SaveIcon from '@mui/icons-material/Save';

export interface IFormsProps {
} 

function Formularios(props: IFormsProps) {
    const [loading, setLoading] = useState(false);
    const [form, setForm] = useState({
        nombre: '',
        radioGroup: '',
        checkBoxValue: false,
        edad: '',
    });

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    const handleChecked = (event: React.ChangeEvent<HTMLInputElement>) => {
        setForm({
            ...form,
            [event.target.name]: event.target.checked
        });
    }

    const handleSelect = (event: SelectChangeEvent) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    const enviarFormulario = (event: React.FormEvent) => {
        event.preventDefault();
        setLoading(true);
        console.log(form);
        setLoading(false);
    }

    return (
        <Box sx={{ maxWidth: '100%' }}>
            <h2>Formularios</h2>
            <form action="" onSubmit={enviarFormulario}>
                <div>
                    <FormControl fullWidth sx={{ mt: 1 }}>
                        <TextField
                            name="nombre"
                            size="small"
                            label="Amount"
                            variant="outlined"
                            value={form.nombre}
                            onChange={handleChange} />
                    </FormControl>
                </div>
                <div>
                    <FormControl fullWidth sx={{ mt: 1 }}>
                        <FormLabel>Gender</FormLabel>
                        <RadioGroup
                            name="radioGroup"
                            value={form.radioGroup}
                            onChange={handleChange}>
                            <FormControlLabel value="female" control={<Radio size="small" />} label="Female" />
                            <FormControlLabel value="male" control={<Radio size="small" />} label="Male" />
                        </RadioGroup>
                    </FormControl>
                </div>
                <div>
                    <Checkbox
                        name="checkBoxValue"
                        checked={form.checkBoxValue}
                        onChange={handleChecked}
                        inputProps={{ 'aria-label': 'controlled' }} />
                </div>
                <div>
                    <FormControl fullWidth sx={{ mt: 1 }}>
                        <InputLabel size="small">Age</InputLabel>
                        <Select
                            name="edad"
                            label="Age"
                            value={form.edad}
                            onChange={handleSelect}
                            size="small">
                            <MenuItem value={10}>Ten</MenuItem>
                            <MenuItem value={20}>Twenty</MenuItem>
                            <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </FormControl>
                </div>
                <div>
                    <LoadingButton
                        sx={{ mt: 1 }}
                        loading={loading}
                        loadingPosition="start"
                        startIcon={<SaveIcon />}
                        variant="outlined"
                        type="submit">
                        Save
                    </LoadingButton>
                </div>
            </form>
        </Box>
    );
}

export { Formularios }