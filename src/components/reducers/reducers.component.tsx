import * as React from 'react';
import ContadorMejoradoComponent from './components/contador-mejorado.component';
import ContadorComponent from './components/contador.component';

interface IReducersComponentProps {
}

const ReducersComponent: React.FC<IReducersComponentProps> = (props) => {
    return (
        <>
            <ContadorComponent />
            <div style={{ marginTop: '1rem' }}>
                <ContadorMejoradoComponent />
            </div>

        </>
    );
};

export default ReducersComponent;
