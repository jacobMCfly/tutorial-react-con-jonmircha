import * as React from 'react';

// ======================================================================
//  Reducer
// ====================================================================== 
const defaultState = {
    contador: 0
};

const init = () => {
    return {
        contador: defaultState.contador + 1
    }
};

const TYPES = {
    INCREMENTAR: 'INCREMENTAR',
    DECREMENTAR: 'DECREMENTAR',
    INCREMENTAR_5: 'INCREMENTAR_5',
    DECREMENTAR_5: 'DECREMENTAR_5',
    RESET: 'RESET',
};

function reducer(state: any, action: { type: string, payload?: any }) {
    switch (action.type) {
        case TYPES.INCREMENTAR:
            return { contador: state.contador + 1 };
        case TYPES.INCREMENTAR_5:
            return { contador: state.contador + action.payload };
        case TYPES.DECREMENTAR:
            return { contador: state.contador - 1 };
        case TYPES.DECREMENTAR_5:
            return { contador: state.contador - action.payload };
        case TYPES.RESET:
            return defaultState;
        default:
            return state;
    }
}
// ======================================================================
//  Componente
// ====================================================================== 
interface IContadorComponentProps {
}

const ContadorComponent: React.FC<IContadorComponentProps> = (props) => {

    const [state, dispatch] = React.useReducer(reducer, defaultState, init);

    const sumar = () => dispatch({ type: TYPES.INCREMENTAR });
    const sumar5 = () => dispatch({ type: TYPES.INCREMENTAR_5, payload: 5 });

    const restar = () => dispatch({ type: TYPES.DECREMENTAR });
    const restar5 = () => dispatch({ type: TYPES.DECREMENTAR_5, payload: 5 });

    const reset = () => dispatch({ type: TYPES.RESET });


    return (
        <div style={{ border: '1px solid', textAlign: 'center' }}>
            <h2>Contador reducer</h2>
            <nav>
                <button onClick={restar}>-</button>
                <button onClick={sumar}>+</button>
                <button onClick={restar5}>-5</button>
                <button onClick={sumar5}>+5</button>
                <div>
                    <button onClick={reset}>resetear</button>
                </div>
            </nav>
            <h3>{state.contador}</h3>
        </div>
    );
};

export default ContadorComponent;