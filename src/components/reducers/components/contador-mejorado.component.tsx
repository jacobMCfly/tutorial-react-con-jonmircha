import * as React from 'react';
import { TYPES } from '../../../reducers/actions/contador.actions';
import { contadorInit, contadorReducer, contadorReducerDefaultState } from '../../../reducers/contador.reducer';


// ======================================================================
//  Componente
// ====================================================================== 
interface IContadorMejoradoComponentProps {
}

const ContadorMejoradoComponent: React.FC<IContadorMejoradoComponentProps> = (props) => {

    const [state, dispatch] = React.useReducer(contadorReducer, contadorReducerDefaultState, contadorInit);

    const sumar = () => dispatch({ type: TYPES.INCREMENTAR });
    const sumar5 = () => dispatch({ type: TYPES.INCREMENTAR_5, payload: 5 });

    const restar = () => dispatch({ type: TYPES.DECREMENTAR });
    const restar5 = () => dispatch({ type: TYPES.DECREMENTAR_5, payload: 5 });

    const reset = () => dispatch({ type: TYPES.RESET });


    return (
        <div style={{ border: '1px solid', textAlign: 'center' }}>
            <h2>Contador Mejorado</h2>
            <nav>
                <button onClick={restar}>-</button>
                <button onClick={sumar}>+</button>
                <button onClick={restar5}>-5</button>
                <button onClick={sumar5}>+5</button>
                <div>
                    <button onClick={reset}>resetear</button>
                </div>
            </nav>
            <h3>{state.contador}</h3>
        </div>
    );
};

export default ContadorMejoradoComponent;