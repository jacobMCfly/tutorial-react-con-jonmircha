import * as React from 'react';
import FooterComponent from './components/footer.component';
import HeaderComponent from './components/header.component';
import MainBodyComponent from './components/main-body.component';
import './components/page.css';

export interface IPageNoContextComponentProps {
}

const initialTheme = 'light';
const initialLanguage = 'es';
const initialAuth = false;

const translations = {
    es: {
        header: {
            title: 'Mi aplicación sin context',
            theme: {
                light: 'Claro',
                dark: 'Oscuro'
            }
        },
        main: {
            welcome: 'Bienvenid@ invitado',
            hello: 'Hola usuari@',
            content: 'Mi contenido principal'
        },
        footer: {
            title: 'Mi footer'
        },
        button: {
            login: 'Iniciar sesión',
            logout: 'Cerrar sesión'
        },
    },
    en: {
        header: {
            title: 'Mi application without context',
            theme: {
                light: 'Light',
                dark: 'Dark'
            }
        },
        main: {
            welcome: 'Welcome guess',
            hello: 'Hello user',
            content: 'My main content'
        },
        footer: {
            title: 'my footer'
        },
        button: {
            login: 'Log in',
            logout: 'Log out'
        },
    }
}

export default function PageNoContextComponent(props: IPageNoContextComponentProps) {
    const [theme, setTheme] = React.useState(initialTheme);
    const [language, setLanguage] = React.useState<keyof typeof translations>(initialLanguage);
    const [texts, setTexts] = React.useState(translations[language]);
    const [autenticado, setAuth] = React.useState(initialAuth)

    const cambiarTema = (e: React.ChangeEvent<any>) => {
        const tema = e.target.value;
        setTheme(tema);
    };

    const cambiarLenguaje = (e: React.ChangeEvent<any>) => {
        const valorEvento = e.target.value as keyof typeof translations;
        setLanguage(valorEvento);
        setTexts(translations[valorEvento]);
    }

    const autenticar = (e: React.ChangeEvent<any>) => {
        setAuth(!autenticado)
    }

    return (
        <div className="my-page">
            <HeaderComponent
                cambiarTema={cambiarTema}
                cambiarLenguaje={cambiarLenguaje}
                theme={theme}
                texts={texts}
                autenticar={autenticar}
                autenticado={autenticado}
            />
            <MainBodyComponent
                theme={theme}
                texts={texts}
                autenticado={autenticado} />
            <FooterComponent
                theme={theme}
                texts={texts} />
        </div>
    );
}
