import * as React from 'react';
import { AuthProvider } from '../../context/auth.context';
import { LanguageProvider } from '../../context/language.context';
import { ThemeCustomProvider } from '../../context/theme.context';
import FooterContextComponent from './components-context/footer-context.component';
import HeaderContextComponent from './components-context/header-context.component';
import MainBodyContextComponent from './components-context/main-body-context.component';
import './components-context/page.css';

export interface IPageContextComponentProps {
}

export default function PageContextComponent(props: IPageContextComponentProps) {

  return (
    <div className="my-page">
      <AuthProvider>
        <ThemeCustomProvider>
          <LanguageProvider>
            <HeaderContextComponent />
            <MainBodyContextComponent />
            <FooterContextComponent />
          </LanguageProvider>
        </ThemeCustomProvider>
      </AuthProvider>
    </div>
  );
}
