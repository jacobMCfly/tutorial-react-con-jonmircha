import * as React from 'react';
import './page.css';

interface IHeaderComponentProps {
    cambiarTema?: (arg: React.ChangeEvent<any>) => void;
    cambiarLenguaje?: (arg: React.ChangeEvent<any>) => void;
    autenticar?: (arg: React.ChangeEvent<any>) => void;
    autenticado?: boolean;
    theme?: string;
    texts: any;
}

const HeaderComponent: React.FC<IHeaderComponentProps> = (props) => {
    return (
        <header className={props.theme}>
            <h2 style={
                {
                    textAlign: 'center',
                    paddingTop: '1rem'
                }
            }>
                {props.texts.header.title}
            </h2>

            <div style={{ textAlign: 'right' }}>
                <select
                    onChange={props.cambiarLenguaje}
                    name="language"
                    style={{ margin: '1rem' }}>
                    <option value="es">ES</option>
                    <option value="en">EN</option>
                </select>
                <input
                    type="radio"
                    name="theme"
                    id="light"
                    value="light"
                    onClick={props.cambiarTema} />
                <label htmlFor="light"> {props.texts.header.theme.light}</label>
                <input
                    type="radio"
                    name="theme"
                    id="dark"
                    value="dark"
                    onClick={props.cambiarTema} />
                <label htmlFor="dark"> {props.texts.header.theme.dark}</label>
                <button
                    style={{ margin: '1rem' }}
                    onClick={props.autenticar}
                >
                    {props.autenticado ? props.texts.button.logout : props.texts.button.login}
                </button>
            </div>
        </header>
    );
};

export default HeaderComponent;
