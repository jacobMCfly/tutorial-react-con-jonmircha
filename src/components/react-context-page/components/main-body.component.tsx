import * as React from 'react';

interface IMainBodyComponentProps {
  autenticado?: boolean;
  theme: string;
  texts: any;
}

const MainBodyComponent: React.FC<IMainBodyComponentProps> = (props) => {
  return (
    <>
      <main className={props.theme} style={{ border: '1px solid', padding: '1rem' }}>
        <p>{props.autenticado ? props.texts.main.hello : props.texts.main.welcome}</p>
        <p>{props.texts.main.content}</p>
      </main>
    </>
  );
};

export default MainBodyComponent;