import * as React from 'react';

interface IFooterComponentProps {
    theme: string;
    texts: any;
}

const FooterComponent: React.FC<IFooterComponentProps> = (props) => {
    return (
        <>

            <footer
                className={props.theme}
                style={{ border: '1px solid', padding: '1rem' }}
            >
                <h3>{props.texts.footer.title}</h3>
            </footer>
        </>
    );
};

export default FooterComponent;