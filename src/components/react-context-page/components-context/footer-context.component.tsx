import * as React from 'react';
import LanguageContext from '../../../context/language.context';
import ThemeCustomContext from '../../../context/theme.context';

interface IFooterContextComponentProps {
}

const FooterContextComponent: React.FC<IFooterContextComponentProps> = (props) => {
    const { theme } = React.useContext(ThemeCustomContext);
    const { texts } = React.useContext(LanguageContext);
    
    return ( 
        <footer
            className={theme}
            style={{ border: '1px solid', padding: '1rem' }}
        >
            <h3>{texts.footer.title}</h3>
        </footer>
    );
};

export default FooterContextComponent;