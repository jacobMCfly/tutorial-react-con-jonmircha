import * as React from 'react';
import AuthContext from '../../../context/auth.context';
import LanguageContext from '../../../context/language.context';
import ThemeCustomContext from '../../../context/theme.context';
import './page.css';

interface IHeaderContextComponentProps {
    autenticar?: (arg: React.ChangeEvent<any>) => void;
    autenticado?: boolean;
}

const HeaderContextComponent: React.FC<IHeaderContextComponentProps> = (props) => {
    const { theme, cambiarTema } = React.useContext(ThemeCustomContext);
    const { texts, cambiarLenguaje } = React.useContext(LanguageContext);
    const { autenticado, autenticar } = React.useContext(AuthContext);

    return (
        <header className={theme}>
            <h2 style={
                {
                    textAlign: 'center',
                    paddingTop: '1rem'
                }
            }>
                {texts.header.title}
            </h2>

            <div style={{ textAlign: 'right' }}>
                <select
                    onChange={cambiarLenguaje}
                    name="language"
                    style={{ margin: '1rem' }}>
                    <option value="es">ES</option>
                    <option value="en">EN</option>
                </select>
                <input
                    type="radio"
                    name="theme"
                    id="light-context"
                    value="light"
                    onClick={cambiarTema} />
                <label htmlFor="light-context"> {texts.header.theme.light}</label>
                <input
                    type="radio"
                    name="theme"
                    id="dark-context"
                    value="dark"
                    onClick={cambiarTema} />
                <label htmlFor="dark-context"> {texts.header.theme.dark}</label>
                <button
                    style={{ margin: '1rem' }}
                    onClick={autenticar}
                >
                    {autenticado ? texts.button.logout : texts.button.login}
                </button>
            </div>
        </header>
    );
};

export default HeaderContextComponent;