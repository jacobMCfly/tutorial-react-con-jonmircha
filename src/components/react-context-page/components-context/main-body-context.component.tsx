import * as React from 'react';
import AuthContext from '../../../context/auth.context';
import LanguageContext from '../../../context/language.context';
import ThemeCustomContext from '../../../context/theme.context';

interface IMainBodyContextComponentProps {
  autenticado?: boolean;
}

const MainBodyContextComponent: React.FC<IMainBodyContextComponentProps> = (props) => {
  const { theme } = React.useContext(ThemeCustomContext);
  const { texts } = React.useContext(LanguageContext);
  const { autenticado } = React.useContext(AuthContext);
  
  return (
    <main className={theme} style={{ border: '1px solid', padding: '1rem' }}>
      <p>{autenticado ? texts.main.hello : texts.main.welcome}</p>
      <p>{texts.main.content}</p>
    </main>
  );
};

export default MainBodyContextComponent;