import React, { useState, useEffect, memo } from 'react';
import { Button } from '@mui/material';

interface Props { }

interface State { }


const PokemonComponent = ({ name, url }: any) => { 
    return (
        <>
            <figure>
                <figcaption>Pokemon: {name}</figcaption>
                {/* <img src={url} alt={name} />   */}
            </figure>
        </>
    )
}

const Hooks: React.FC = memo((props: Props) => {
    console.log('ejecutando Hooks component')
    const [contador, setContador] = useState(0)
    const [pokemons, setPokemons] = useState([])

    useEffect(() => {
        if (contador !== 0) {
            pokemonData()
        }
    }, [contador]);

    useEffect(() => {
        pokemonData()
    }, []);

    const pokemonData = async () => {
        const data = await fetch('https://pokeapi.co/api/v2/pokemon/').then((response) => response.json())
        if (data) {
            setPokemons(data.results)
        }
    }

    return (
        <>
            <div style={{ border: '1px solid', borderColor: 'red', padding: '1rem', marginTop: '1rem' }}>


                <div>contador desde hook : {contador}</div><br />
                <Button variant="contained" onClick={() => setContador(contador + 1)}>Hook</Button><br />
                {pokemons.length === 0 ? (<p>cargando</p>) :
                    (
                        pokemons.map((pokemon: any, index: number) => {
                            return <PokemonComponent key={index} name={pokemon.name} url={pokemon.url} />
                        })
                    )
                }
            </div>
        </>

    )
});



export { Hooks }