import React, { Component } from 'react'; 

interface Props {
    msg?: string;
}

interface State { }


const ComponenteHijo: React.FC<Props> = ({ msg }: Props) => {
    return (
        <div>Hola desde el componente hijo, {msg}</div>
    )
}

export default class Estado extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        console.log(props)
    }

    public static defaultProps: Props = {};

    state: State = {};

    render() {
        return (
            <div>
                <ComponenteHijo msg="Gola" />
            </div>
        )
    }
} 