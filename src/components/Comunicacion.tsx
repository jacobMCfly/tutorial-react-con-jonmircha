import React, { Component } from 'react';
import { Button } from '@mui/material'

interface Props { }

interface State {
    contador: number;
}

export class Comunicacion extends Component<Props, State> {

    public static defaultProps: Props = {}

    state: State = {
        contador: 0
    }

    incrementarContador = () => {
        this.setState({
            contador: this.state.contador + 1
        })
    }

    render() {
        return (
            <>
                <h2>Comunicacion entre componentes</h2>
                <Hijo incrementarContador={this.incrementarContador} msg="Información desde el padre" />
                <h2>Contador {this.state.contador}</h2>
            </>
        )
    }
}

// Propiedades default en componente funcional


interface IPropiedadesHijo {
    msg: string;
    incrementarContador: () => void
}

const defaultProps: IPropiedadesHijo = {
    msg: 'Default',
    incrementarContador: () => console.log('Propiedad default')
}

const Hijo: React.FC<IPropiedadesHijo> = (props) => {
    return (
        <>
            <h2>{props.msg}</h2>
            <Button variant="contained" onClick={props.incrementarContador}> Incrementar contador desde el hijo </Button>
        </>
    )
}
Hijo.defaultProps = defaultProps;



