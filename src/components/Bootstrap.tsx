import React, { Component } from 'react'
import { Box } from '@mui/material'

interface Props { }

interface State {
    count: number;
}

class Bootstrap extends Component<Props, State> {
    state: State = {
        count: 0
    }

    componentDidUpdate(prevProps: Props, prevState: State) {
        if (prevState.count !== this.state.count) {
            console.log('se actualiza')
        }
    }

    componentDidMount() {
        console.log('se renderizara')
    }



    render() {
        return (
            <Box sx={{ mt: 1 }}>
                <div style={{ backgroundColor: 'red' }} onClick={() => this.setState({ count: this.state.count + 1 })}> Bootstrap </div>
                <div>{this.state.count}</div>
            </Box>
        )
    }
}

export { Bootstrap } 