import * as MuiIcons from '@mui/icons-material'

// eslint-disable-next-line import/no-anonymous-default-export
export default ({ iconName, ...props }: any) => {
    const Icon = MuiIcons[iconName as keyof typeof MuiIcons]
    return (<Icon {...props} />)
}

