import React, { useCallback, useState } from 'react';
import { Hooks } from './Hooks';

interface Props {
    msg?: string;
}

interface State {
    count: number;
} 

const Componente: React.FC = (props: Props) => {
    console.log('renderizando')
    const [state, setState] = useState<State>({
        count: 0,
    })

    const contador = (): any => {
        setState({ count: state.count + 1 })
    }

    useCallback(
        () => {
            contador();
        },
        [state.count],
    )

    // Render

    let { msg } = props;
    let { count } = state;

    return (

        <>
            <h2> {msg}</h2>
            <h2> {count}</h2>

            <button onClick={contador}> Estado </button>

            <Hooks></Hooks>

        </>
    );

}

export default Componente;



