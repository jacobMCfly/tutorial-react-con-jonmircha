import * as React from 'react';

interface IUseMemoComponentProps { }

const defaultProps: IUseMemoComponentProps = {}

const UseMemoComponent: React.FC<IUseMemoComponentProps> = (props) => {
    console.log('renderizando componente')
    const [count, setCount] = React.useState(0)

    const superNumero = React.useMemo(() => {
        let numero = 0;
        for (let i = 0; i < 100000; i++) {
            numero++;
            console.log(numero)
        }
        return numero;
    }, []);

    return (
        <>

            <div style={{ textAlign: 'center', border: '1px solid', marginTop: '1em' }}>
                <h2 >Contador para renderizar componente de nuevo</h2>
                <div>
                    <button onClick={() => setCount(count + 1)}>+</button>
                    <button onClick={() => setCount(count - 1)}>-</button>
                </div>
                <h3>{count}</h3>
            </div>

            <div style={{ textAlign: 'center', marginTop: '1em', border: '1px solid', borderColor: 'red' }}>
                <h2> Propiedad Computada:</h2>
                <h3>{superNumero}</h3>
            </div>

        </>
    )
};

UseMemoComponent.defaultProps = defaultProps;


export default UseMemoComponent;
