import * as React from 'react';
import CarritoDeComprasComponent from './components/carrito-de-compras/carrito-de-compras.component';

interface ICCarritoComponentProps { }

const CarritoComponent: React.FC<ICCarritoComponentProps> = (props) => {
    return (

        <CarritoDeComprasComponent />

    );
};

export default CarritoComponent;
