import * as React from 'react';
import './producto.component.css';

interface IProductoComponentProps {
    data: {
        id?: string | number;
        nombre: string;
        precio: string | number;
    };
    agregarProductoAlCarrito: (id?: string | number) => void;
}
const ProductoComponent: React.FC<IProductoComponentProps> = ({ data, agregarProductoAlCarrito }) => {
    let { id, nombre, precio } = data;
    return (

        <div className='tarjeta'>
            <h4>{nombre}</h4>
            <h5>${precio}.00</h5>
            <button onClick={() => agregarProductoAlCarrito(id)}>Agregar</button>
        </div>
    );
};

export default ProductoComponent;
