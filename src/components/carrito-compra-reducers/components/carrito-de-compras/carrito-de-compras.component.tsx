import * as React from 'react';
import { TYPES } from '../../../../reducers/actions/compras.actions';
import { comprasDefaultState, comprasReducer } from '../../../../reducers/compras.reducer';
import CarritoArticuloComponent from '../carrito-articulo/carrito-articulo.component';
import ProductoComponent from '../producto/producto.component';
import './carrito-de-compras.component.css';

interface ICarritoDeComprasComponentProps {
}

const CarritoDeComprasComponent: React.FC<ICarritoDeComprasComponentProps> = (props) => {
    const [state, dispatch] = React.useReducer(comprasReducer, comprasDefaultState);
    const { productos, carrito } = state;

    const agregarProductoAlCarrito = (id?: string | number) => {
        dispatch({
            type: TYPES.AGREGAR_PRODUCTO_AL_CARRITO,
            payload: id
        });
    };

    const eliminarProductoDelCarrito = (id?: string | number, todos = false) => {
        if (todos) {
            dispatch({ type: TYPES.ELIMINAR_TODO_DEL_CARRITO, payload: id });
        } else {
            dispatch({ type: TYPES.ELIMINAR_PRODUCTO_DEL_CARRITO, payload: id });
        }
    };

    const limpiarCarrito = () => {
        dispatch({ type: TYPES.LIMPIAR_CARRITO });
    };

    return (
        <>
            <div style={{ textAlign: 'center', marginTop: '1rem' }}>
                <h2>Carrito de compras</h2>
                <h3>Productos</h3>
                <article className="box grid-responsive">
                    {productos.map(
                        (producto: any) =>
                            <ProductoComponent
                                key={producto.id}
                                data={producto}
                                agregarProductoAlCarrito={agregarProductoAlCarrito}
                            />
                    )}
                </article>
                <h3>Carrito</h3>
                <article className="box">
                    <button onClick={limpiarCarrito}>Limpiar carrito</button>
                    {carrito.map((articulo: any) =>
                        <CarritoArticuloComponent
                            key={articulo.id}
                            data={articulo}
                            eliminarProductoDelCarrito={eliminarProductoDelCarrito}
                        />
                    )}
                </article>
            </div>

        </>
    );
};

export default CarritoDeComprasComponent;
