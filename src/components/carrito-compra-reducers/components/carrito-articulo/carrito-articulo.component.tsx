import * as React from 'react';
import './carrito-articulo.component.css';

interface ICarritoArticuloComponentProps {
  data: {
    id?: string | number;
    nombre: string;
    precio: number;
    cantidad?: number;
  };
  eliminarProductoDelCarrito: (id?: string | number, todos?: boolean) => void;
  limpiarCarrito?: () => void;
}

const CarritoArticuloComponent: React.FC<ICarritoArticuloComponentProps> = ({ data, eliminarProductoDelCarrito }) => {
  const { id, nombre, precio, cantidad } = data;

  return (
    <div className="articulo">
      <h4>{nombre}</h4>
      <h5>${precio}.00</h5>
      <h4>Cantidad: &nbsp;{cantidad} = ${cantidad ? precio * cantidad : 0}.00</h4>
      <button onClick={() => eliminarProductoDelCarrito(id, false)}>Eliminar producto</button>
      <button onClick={() => eliminarProductoDelCarrito(id, true)}>Eliminar todos</button>
    </div>
  );
};

export default CarritoArticuloComponent;
