import React, { useRef } from 'react';

interface IReferenciasProps { }

function Referencias(props: IReferenciasProps) {
    let refMenuBtn = useRef() as any;
    let refMenu = useRef() as any;
    // console.log(refMenu, refMenuBtn);

    const handleToggleMenu = (e: any) => {
        // const $menu = document.getElementById('menu') as HTMLElement;
        if (refMenuBtn.current.textContent === 'Menú') {
            refMenuBtn.current.textContent = 'Cerrar'
            refMenu.current.style.display = 'block';
        } else {
            refMenuBtn.current.textContent = 'Menú'
            refMenu.current.style.display = 'none';
        }
    };

    return (
        <>
            <h2>
                Referencias
            </h2>

            <button ref={refMenuBtn} onClick={handleToggleMenu}>Menú</button>
            <nav ref={refMenu} style={{ display: 'none' }}>
                <a href="#">Sección 1</a> <br />
                <a href="#">Sección 2</a> <br />
                <a href="#">Sección 4</a> <br />
            </nav>
        </>
    );
}

export { Referencias }