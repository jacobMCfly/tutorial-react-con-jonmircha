import { LoadingButton } from '@mui/lab';
import { Box, Checkbox, FormControl, FormControlLabel, FormLabel, InputLabel, MenuItem, Radio, RadioGroup, Select, TextField, FormGroup, DialogActions, Button } from '@mui/material';
import * as React from 'react';
import { useForm } from '../hooks/useForm';
import SaveIcon from '@mui/icons-material/Save';

export interface IFormularioHooksProps {
    onClose?: () => void;
}

export interface IFormularioHooksState {
    form: any;
    errores: any,
    cargando: boolean;
    respuesta: any;
    formEnviado: boolean;
    formValido: boolean;
    handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    handleChecked: (e: React.ChangeEvent<HTMLInputElement>) => void;
    handleSelect: (args: any) => void;
    handleBlur: (e: React.FocusEvent<HTMLElement, Element>) => void;
    handleSubmit: (e: React.FormEvent<Element>) => void;
}

interface IFormulario {
    id?: number;
    nombre: string;
    radioGroup: string | number;
    checkBoxValue: boolean;
    edad: string;
}

const formularioInicial: IFormulario = {
    id: undefined,
    nombre: '',
    radioGroup: '',
    checkBoxValue: false,
    edad: '',
};

const validacionForm = (form: IFormulario) => {
    let errores = {} as IFormulario;

    if (!form.nombre) {
        errores.nombre = 'El nombre es requerido';
    } else if (form.nombre.length < 10) {
        errores.nombre = 'El nombre es menor a 10 caracteres';
    }

    return errores;
}

export function FormularioHooks(props: IFormularioHooksProps) {
    const {
        form,
        errores,
        cargando,
        respuesta,
        formEnviado,
        formValido,
        handleChange,
        handleChecked,
        handleSelect,
        handleBlur,
        handleSubmit
    }: IFormularioHooksState = useForm(formularioInicial, validacionForm, 'usuario');
    const { onClose } = props;

    return (

        <Box sx={{ maxWidth: '100%' }}>
            <form action="" onSubmit={handleSubmit}>
                <div>
                    <FormControl fullWidth sx={{ mt: 1 }}>
                        <TextField
                            error={errores.nombre ? true : false}
                            helperText={errores.nombre}
                            name="nombre"
                            size="small"
                            label="Amount"
                            variant="outlined"
                            value={form.nombre}
                            onBlur={handleBlur}
                            onChange={handleChange} />
                    </FormControl>
                </div>
                <div>
                    <FormControl fullWidth sx={{ mt: 1 }}>
                        <FormLabel>Gender</FormLabel>
                        <RadioGroup
                            name="radioGroup"
                            value={form.radioGroup}
                            onBlur={handleBlur}
                            onChange={handleChange}>
                            <FormControlLabel value="female" control={<Radio size="small" />} label="Female" />
                            <FormControlLabel value="male" control={<Radio size="small" />} label="Male" />
                        </RadioGroup>
                    </FormControl>
                </div>
                <div>
                    <FormControl fullWidth sx={{ mt: 1 }}>
                        <FormGroup>
                            <FormControlLabel
                                control={<Checkbox name="checkBoxValue" checked={form.checkBoxValue} onBlur={handleBlur} onChange={handleChecked} />}
                                label={'Probando label'}
                            />
                        </FormGroup>
                    </FormControl>

                </div>
                <div>
                    <FormControl fullWidth sx={{ mt: 1 }}>
                        <InputLabel size="small">Age</InputLabel>
                        <Select
                            name="edad"
                            label="Age"
                            value={form.edad}
                            onChange={handleSelect}
                            size="small">
                            <MenuItem value={10}>Ten</MenuItem>
                            <MenuItem value={20}>Twenty</MenuItem>
                            <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </FormControl>
                </div>
                <Box textAlign='end'>
                    <Button variant="outlined" sx={{ mt: 4, mr: 1 }} color="error" onClick={onClose}> Cancelar</Button>
                    <LoadingButton
                        disabled={!formValido}
                        sx={{ mt: 4 }}
                        loading={cargando}
                        loadingPosition="start"
                        startIcon={<SaveIcon />}
                        variant="contained"
                        type="submit">
                        Save
                    </LoadingButton>
                </Box>


            </form>
        </Box>
    );
}

// interface IAccionesFormulario {
//     dialog: boolean;
//     formValido: boolean;
//     cargando: boolean;
// }

// export function AccionesFormulario(props: IAccionesFormulario) {
//     const { dialog, formValido, cargando } = props
//     return (<>
//         {dialog === true ? (
//             <DialogActions>
//                 <LoadingButton
//                     disabled={!formValido}
//                     sx={{ mt: 1 }}
//                     loading={cargando}
//                     loadingPosition="start"
//                     startIcon={<SaveIcon />}
//                     variant="outlined"
//                     type="submit">
//                     Save
//                 </LoadingButton>
//             </DialogActions>
//         ) : (
//             <LoadingButton
//                 disabled={!formValido}
//                 sx={{ mt: 1 }}
//                 loading={cargando}
//                 loadingPosition="start"
//                 startIcon={<SaveIcon />}
//                 variant="outlined"
//                 type="submit">
//                 Save
//             </LoadingButton>
//         )}
//     </>)
// }
