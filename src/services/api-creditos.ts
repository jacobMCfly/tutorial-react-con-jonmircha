import { Capacitor } from '@capacitor/core';
import axios from 'axios';
import { config } from '../constants/constants';


let baseUrl: string;
let aplicacion: string;
// Esta condición es para cuando corre la aplicación en emulador de android.
// El emulador no permite hacer las llamadas a la api localhost ya que la app en capacitor corre en el esquema http://localhost y regresa ERR:CONNECTION REFUSED.
// Por lo que al correr el emulador de Android Studio, hace las llamadas sustituyendo esta ip especial https://developer.android.com/studio/run/emulator-networking.html
// En producción no hay ningun problema, mientras las llamadas contengan https
// Para iOS en producción o en test no hay condiciones especiales


// axios.defaults.headers.common.Authorization = `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjYwNzY2NTEwLCJpYXQiOjE2NTgxNzQ1MTAsImp0aSI6ImUyNWQ4N2E5ODc2ZTQ5NTZhZjc0ZDg1NDI5YmI3OGYyIiwidXNlcl9pZCI6MX0.ENWv7P-5WLg3o5YZ2R2QQLEotDnLGVt85Qw34Egqa9I`

if (Capacitor.isNativePlatform() && Capacitor.getPlatform() === 'android' && config.produccion === false) {
    baseUrl = 'http://10.0.2.2:8000';
    aplicacion = config.aplicacion.creditos;
} else {
    baseUrl = config.url.API_URL;
    aplicacion = config.aplicacion.creditos;
}

const generarQueryParams = (params: any) => {
    params = Object.entries(params)
        .map(([key, value]) => {
            return `${key}=${value}`
        })
        .join('&')

    return params ? `?${params}` : ''
}

const lista = async (modelo: any, filtro?: any, sourceToken?: any) => {
    const filtro_ = generarQueryParams(filtro || {})
    const url = `${baseUrl}${aplicacion}${modelo}/${filtro_}`
    return await axios.get(url, {
        cancelToken: sourceToken ? sourceToken.token : undefined
    })
        .catch(function (error) {
            // manejarError(error)
        })
}

const objeto = async (modelo: any, id: any) => {
    const url = `${baseUrl}${aplicacion}${modelo}/${id}/`
    return await axios.get(url)
        .catch(function (error) {
            // manejarError(error)
        })
}

const guardar = async (modelo: any, obj: any) => {
    const url = `${baseUrl}${aplicacion}${modelo}/`
    return await axios.post(url, obj)
        .catch(function (error) {
            // manejarError(error)
        })
}

const actualizar = async (modelo: any, obj: any) => {
    const url = `${baseUrl}${aplicacion}${modelo}/${obj.id}/`
    return await axios.put(url, obj)
        .catch(function (error) {
            // manejarError(error)
        })
}

const eliminar = async (modelo: any, obj: any) => {
    const url = `${baseUrl}${aplicacion}${modelo}/${obj.id}/`
    return await axios.delete(url)
        .catch(function (error) {
            // manejarError(error)
        })
}

const get = async (endPoint: any, sourceToken?: any) => {
    return await axios.get(`${baseUrl}${endPoint}`, {
        cancelToken: sourceToken ? sourceToken.token : undefined
    })
        .catch(function (error) {
            console.log(error)
            // manejarError(error)
        })
}

const getUrlCompleta = async (url: any, sourceToken?: any) => {
    return await axios.get(`${url}`, {
        cancelToken: sourceToken ? sourceToken.token : undefined
    })
        .catch(function (error) {
            // manejarError(error)
        })
}

const post = async (endPoint: any, objeto: any) => {
    const url = `${baseUrl}/${endPoint}/`
    return await axios.post(url, objeto)
        .catch(function (error) {
            // manejarError(error)
        })
}

const generarTokenPeticion = () => {
    return axios.CancelToken.source()
}



const api = {
    lista,
    objeto,
    guardar,
    actualizar,
    eliminar,
    get,
    post,
    getUrlCompleta,
    generarTokenPeticion,
    baseUrl
}

export { api }