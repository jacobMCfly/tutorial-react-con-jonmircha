import { SelectChangeEvent } from '@mui/material';
import React, { useState } from 'react';

export const useForm = (initialForm: any, validateForm: (form: any) => any, modelo?: string) => {
    const [form, setForm] = useState<any>(initialForm);
    const [errores, setErrores] = useState<any>({});
    const [cargando, setCargando] = useState<boolean>(false);
    const [respuesta, setResponse] = useState(null);
    const [formEnviado, setFormEnviado] = useState(false);
    const [formValido, setFormValido] = useState(false);

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        });
        const _errores = validateForm(form);
        setErrores(_errores);
        Object.keys(_errores).length === 0 ? setFormValido(true) : setFormValido(false)
    };

    const handleChecked = (e: React.ChangeEvent<HTMLInputElement>) => {
        setForm({
            ...form,
            [e.target.name]: e.target.checked
        });
        const _errores = validateForm(form);
        setErrores(_errores);
        Object.keys(_errores).length === 0 ? setFormValido(true) : setFormValido(false)
    };

    const handleSelect = (e: SelectChangeEvent) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        });
        const _errores = validateForm(form);
        setErrores(_errores);
        Object.keys(_errores).length === 0 ? setFormValido(true) : setFormValido(false)
    };

    const handleBlur = (e: React.FocusEvent<HTMLElement>) => {
        const _errores = validateForm(form);
        setErrores(_errores);
        Object.keys(_errores).length === 0 ? setFormValido(true) : setFormValido(false)
    };

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        const _errores = validateForm(form);
        setErrores(_errores);

        if (Object.keys(_errores).length === 0) {
            setCargando(true);
            setFormValido(true);
            if (form.id) {
                console.log('actualiza el modelo', modelo);
                setFormEnviado(true);
                setCargando(false);
            } else {
                console.log('crea el modelo', modelo)
                setFormEnviado(true);
                setCargando(false);
            }
        } else {
            setFormValido(false);
            return;
        }
    };

    return { form, errores, cargando, respuesta, formEnviado, formValido, handleChange, handleChecked, handleSelect, handleBlur, handleSubmit };
}