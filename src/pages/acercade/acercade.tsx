import React from 'react'
import { Grid, Button, Container, Stack, Box, AppBar, Toolbar, Typography, IconButton } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import MenuIcon from '@mui/icons-material/ArrowBack';
import { useNavigate } from 'react-router-dom';

interface IAcercaDeProps { }

const theme = createTheme();

const AcercaDePage: React.FC = (props: IAcercaDeProps) => {
    const history = useNavigate()
    return (
        <>
            <AppBar position="relative">
                <Toolbar>
                    <IconButton
                        onClick={() => history(-1)}
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" color="inherit" noWrap>
                        Acerca de
                    </Typography>
                </Toolbar>
            </AppBar>
        </>
    )
}

export default AcercaDePage;