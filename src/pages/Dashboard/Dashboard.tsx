import React, { useState, useEffect } from 'react';
import { Grid, Button, Container, Stack, Box, AppBar, Toolbar, Typography, IconButton } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import MenuIcon from '@mui/icons-material/ArrowBack';
import { useNavigate } from 'react-router-dom';

const theme = createTheme();

const Dash: React.FC = (props: any) => {
    const [prevPath, setPrevPath] = useState({})
    const history = useNavigate()

    useEffect(() => {
        console.log(props)
        // if (nextProps.location !== props.location) {
        //     setPrevPath({ prevPath: props.location })
        //     console.log(prevPath)
        // }
    }, [props]);


    return (<>
        <AppBar position="relative">
            <Toolbar>
                <IconButton
                    onClick={() => history(-1)}
                    color="inherit"
                    aria-label="open drawer"
                    edge="start"
                >
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" color="inherit" noWrap>
                    Dash
                </Typography>
            </Toolbar>
        </AppBar>
        <Container maxWidth="md" component="main" sx={{ pt: 1 }}>
            <Grid container direction="row">
                <Box sx={{ width: '100%' }}>
                </Box>
            </Grid>
        </Container>
    </>
    );
};

export default Dash;
