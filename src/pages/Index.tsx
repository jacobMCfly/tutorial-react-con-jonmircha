import React from 'react';
import { Grid, Button, Container, Stack, Box, AppBar, Toolbar, Typography, Icon } from '@mui/material';
import { Outlet, useNavigate } from 'react-router-dom';
import CameraIcon from '@mui/icons-material/PhotoCamera';
import CssBaseline from '@mui/material/CssBaseline';
import Inicio from './inicio/inicio';
import Componente from '../components/Componente';
import ComponentWithIcon from '../components/ComponentWithIcon';
import PageContextComponent from '../components/react-context-page/page-context.component';
import PageNoContextComponent from '../components/react-context-page/page-no-context.component';
import ReducersComponent from '../components/reducers/reducers.component';
import CarritoComponent from '../components/carrito-compra-reducers/carrito.component'; 
import CarritoContextReducersComponent from '../components/carrito-compra-context-reducers/carrito-compra-context-reducers.component';
import ReduxComponent from '../components/redux/redux.component';

interface IIntexProps { }

interface IIndexState { }

const Index: React.FC = (props: IIntexProps) => {
    const history = useNavigate(); 
    return (
        <>
            <AppBar position="relative">
                <Toolbar>
                    <CameraIcon sx={{ mr: 2 }} />
                    <Typography variant="h6" color="inherit" noWrap>
                        App  
                    </Typography>
                </Toolbar>
            </AppBar>
            <Container maxWidth="md" component="main" sx={{ pt: 1 }}>
                <Grid container direction="row">
                    <Box sx={{ width: '100%' }}>
                        {/* <Stack spacing={1}
                            direction="row"
                            sx={{ border: '0px solid' }} >
                            <Button onClick={(e: any) => {
                                e.preventDefault();
                                history('/inicio/dashboard');
                            }}
                                sx={{ width: '100%' }}
                                variant="contained"
                                color="primary"
                                disableElevation
                            >
                                Dash
                            </Button>
                            <Button onClick={(e: any) => {
                                e.preventDefault();
                                history('/acercade');
                            }}
                                sx={{ width: '100%' }}
                                variant="contained"
                                color="primary"
                            >
                                Acerca de
                            </Button>
                        </Stack> */}
                        {/* Componentes */}
                        {/* <Componente></Componente> */}
                        {/* <UseMemoComponent /> */}
                        {/* <ComponentWithIcon iconName="Add" color="primary" /> */}
                        {/* <PageNoContextComponent />
                        <PageContextComponent /> */}
                        {/* <ReducersComponent /> */}
                        {/* <CarritoComponent />
                        <CarritoContextReducersComponent /> */}
                        <ReduxComponent />
                    </Box> 
                </Grid>
            </Container>
        </>
    );
}

export default Index;
