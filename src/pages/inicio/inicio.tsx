import React from 'react';
import { Grid, Button, Container, Stack, Box, AppBar, Toolbar, Typography } from '@mui/material';
import { Outlet, useNavigate } from 'react-router-dom';
import CameraIcon from '@mui/icons-material/PhotoCamera';
import CssBaseline from '@mui/material/CssBaseline';

interface IInicioProps { }

const Inicio: React.FC = (props: IInicioProps) => {
    const history = useNavigate();
    return (
        <>
            <AppBar position="relative">
                <Toolbar>
                    <CameraIcon sx={{ mr: 2 }} />
                    <Typography variant="h6" color="inherit" noWrap>
                        Album layout
                    </Typography>
                </Toolbar>
            </AppBar>
            <Container maxWidth="md" component="main" sx={{ pt: 1 }}>
                <Grid container direction="row">
                    <Box sx={{ width: '100%' }}>
                        <Stack spacing={1}
                            direction="row"
                            sx={{ border: '0px solid' }} >
                            <Button onClick={(e: any) => {
                                e.preventDefault();
                                history('/inicio/dashboard');
                            }}
                                sx={{ width: '100%' }}
                                variant="contained"
                                color="primary"
                                disableElevation
                            >
                                Dash
                            </Button>
                            <Button onClick={(e: any) => {
                                e.preventDefault();
                                history('/acercade');
                            }}
                                sx={{ width: '100%' }}
                                variant="contained"
                                color="primary"
                            >
                                Acerca de
                            </Button>
                        </Stack>
                    </Box>
                </Grid>
            </Container>
        </>
    )
}

export default Inicio;