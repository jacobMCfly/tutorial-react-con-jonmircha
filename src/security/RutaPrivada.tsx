import * as React from 'react';
import { Navigate, Route, useLocation } from 'react-router-dom';


function RutaPrivada({ element }: { element: React.FC | JSX.Element | React.Component }): JSX.Element {
    // let auth = useAuth(); Hook personalizado
    let auth = false;
    let location = useLocation();

    if (!auth) {
        return (
            <>
                <Navigate to="/login" state={{ from: location }} replace />
            </>
        );
    }

    return (<> {element} </>);
}

export default RutaPrivada;